# Weekly Updates

Every Monday an update report is published to
[devtalk](https://devtalk.blender.org/c/announcements/weekly-updates/14).
This is the one document to direct anyone interested on finding about
what is happening in Blender.

This report is compiled by multiple hands:

- Announcements: Everyone.
- Modules and Projects: Modules and project members.
- New Features and Changes:
  - The list of commits is organized manually, but cleanup and fix
    commits are filtered out automatically with this script: `git log
    --no-merges --since 7.days.ago --pretty=format:"%h%x09%ar%x09%s
    (\[commit\](https://projects.blender.org/blender/blender/commit/%h))
    (_%an_)" \| grep -v "Fix " \| grep -v "Cleanup:"`
- Welcomes, final edit and publishing: Development Coordinator (*Thomas
  Dinges*).

## Weekly Reports

Developers funded by the Blender Foundation are expected to post weekly reports
about their work. These are compiled in the weekly updates post.
