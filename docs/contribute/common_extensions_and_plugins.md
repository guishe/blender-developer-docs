# Common Extensions and Plugins

A number of Markdown extensions and (Material for) MkDocs plugins are used to
enrich the experience of this website.

The following isn't a complete list of the available extensions/plugins (for
that, check the `mkdocs.yml` configuration file in the root of the documentation
repository), only the most useful ones are listed and briefly introduced here.

## Admonitions/callouts

Both the
[`admonitions`](https://squidfunk.github.io/mkdocs-material/reference/admonitions/)
and the
[`markdown-callouts`](https://oprypin.github.io/markdown-callouts/index.html)
extensions are used to provide support for content blocks such as these:

=== "Result"
    INFO: **Some Info Box**
    This is an info box.
=== "Markdown (`admonitions`)"

    ```md
    !!! INFO "Some Info Box"
        This is an info box.
    ```
=== "Markdown (`callouts`)"

    ```md
    INFO: **Some Info Box**
    This is an info box.
    ```

    Alternatively (allows multiple paragraphs):
    ```md
    > INFO: **Some Info Box**
    > This is an info box.
    ```

As you can see the extensions use a different syntax, but both give the same
result. Allowing either syntax just makes sure developers can use whatever
syntax they are used to from other platforms.

## Definition lists

Technical documentation needs clear, unambiguous language, especially when
covering complex topics. Clearly defined terminology is important for that. When
used consistently, mentally mapping documentation to the actual implementation
becomes easier. Therefore the documentation makes good use of the [definition
lists](https://squidfunk.github.io/mkdocs-material/setup/extensions/python-markdown/#definition-lists)
extension.

Here's an example taken from its documentation:
=== "Result"
    `Lorem ipsum dolor sit amet`

    :   Sed sagittis eleifend rutrum. Donec vitae suscipit est. Nullam tempus
        tellus non sem sollicitudin, quis rutrum leo facilisis.

    `Cras arcu libero`

    :   Aliquam metus eros, pretium sed nulla venenatis, faucibus auctor ex. Proin
        ut eros sed sapien ullamcorper consequat. Nunc ligula ante.

        Duis mollis est eget nibh volutpat, fermentum aliquet dui mollis.
        Nam vulputate tincidunt fringilla.
        Nullam dignissim ultrices urna non auctor.
=== "Markdown"
    ```
    `Lorem ipsum dolor sit amet`

    :   Sed sagittis eleifend rutrum. Donec vitae suscipit est. Nullam tempus
        tellus non sem sollicitudin, quis rutrum leo facilisis.

    `Cras arcu libero`

    :   Aliquam metus eros, pretium sed nulla venenatis, faucibus auctor ex. Proin
        ut eros sed sapien ullamcorper consequat. Nunc ligula ante.

        Duis mollis est eget nibh volutpat, fermentum aliquet dui mollis.
        Nam vulputate tincidunt fringilla.
        Nullam dignissim ultrices urna non auctor.
    ```