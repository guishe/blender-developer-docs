# Blender 4.2: User Interface

## General

- Support for dropping strings & URL's into the text editor & Python console
  (blender/blender@64843cb12d543f22be636fd69e9512f995c93510)
- Support for horizontal rules in vertical layouts, vertical rules in horizontal. (blender/blender@4c50e6992f)
- Improved property dialogs. (blender/blender@8fee7c5fc7, blender/blender@da378e4d30, blender/blender@615edb3f3c)
- Changes to menu separator padding. (blender/blender@48dee674f3)
- Fixed pixel shift in Windows clipboard image paste. (blender/blender@a677518cb5)
- Improved square color picker when shown very wide. (blender/blender@857341a7a7)
- Improved large confirmation dialogs. (blender/blender@00b4fdce2c, blender/blender@6618755912, blender/blender@3bacd93ba6, blender/blender@2c8a3243ee, blender/blender@5eda7c9fd3, blender/blender@311e0270dd, blender/blender@d3798970199a6b58efc3701785829d17561ea352)
- Improved small confirmation dialogs. (blender/blender@443ea628c5)
- Outliner "Orphan Data" mode renamed to "Unused Data". (blender/blender@72be662af4)
- Cleanup menu gets new "Manage Unused Data" operation. (blender/blender@f04bd961fd)
- Improved User Interface scaling. (blender/blender@85762d99ef, blender/blender@f85989f5db, blender/blender@196cd48aad, blender/blender@23aaf88233, blender/blender@1f1fbda3ee)
- Image Copy and Paste now works in Linux/Wayland. (blender/blender@692de6d380)
- Icon changes. (blender/blender@42e7a720c9, blender/blender@4e303751ff)
- Tooltip changes. (blender/blender@3d85765e14, blender/blender@08cd80a284)
- Preferences "Translation" section changed to "Language". (blender/blender@73d76d4360)
- Improved operators for adding/deleting custom themes. (blender/blender@a0a2e7e0dd)
- Improved operators for adding/deleting custom key configurations. (blender/blender@57729aa8bb)

### Unused IDs Purge UI Improvements

The purge operation now pops-up a dialog where user can choose which options to apply,
and get instant feedback on the amounts of unused data-blocks will be deleted. blender/blender!117304, blender/blender@0fd8f29e88

![New Purge UI popup](ui_new_purge_popup.png)


