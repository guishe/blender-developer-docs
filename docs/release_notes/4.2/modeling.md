# Blender 4.2: Modeling & UV

## UV Editing

### Transform

- The Edge and Vert Slide operators have been ported to the UV Editor
  (blender/blender@aaadb5005e).
  
- The `Set Snap Base` feature (key `B` ) have been enabled in the UV Editor
  (blender/blender@a56a975760).
