# Blender 4.2: Sculpt, Paint, Texture

- Add *Lasso Hide* tool.
  (blender/blender@68afd225018e59d63431e02def51575bfc76f5cb).

<figure>
<video src="../../../videos/4.2_sculpt_lasso_hide.mp4" title="Demo of Lasso Hide tool" width="400" controls=""></video>
<figcaption>Demo of Lasso Hide tool</figcaption>
</figure>
