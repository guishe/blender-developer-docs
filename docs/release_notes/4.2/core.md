# Blender 4.2: Core

## Command Line Arguments

- Running in background-mode using `-b` or `--background` now disables audio by default
  (blender/blender@7c90018f2300646dbdec2481b896999fe93e6e62).

## Undo

- Faster undo due to implicit sharing (blender/blender@0e8e219d71cd27cf025a9920ac4fb54ff7c178b3).