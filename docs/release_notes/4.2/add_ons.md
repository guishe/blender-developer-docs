# Blender 4.2: Add-ons

## 3DS

- Import is now possible by drag & drop from desktop window. (blender/blender-addons@79077f3b7f)
- Multiple files can now be imported by selecting with shift or ctrl. (blender/blender-addons!105227)
- Added option to create a new collection for the imported file. (blender/blender-addons!105232)