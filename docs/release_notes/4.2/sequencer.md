# Blender 4.2: Sequencer

## Performance

- Reduced stalls when new movie clips start playing by caching FFmpeg RGB<->YUV conversion contexts
  (blender/blender@ffbc90874b4f) and reducing amount of redundant work that is done for FFmpeg
  initialization
  (blender/blender@b261654a931).
- Reduced amount of temporary image clears when rendering VSE effect stack
  (blender/blender@b4c6c69632)
- VSE already had an optimization where an Alpha Over strip that is known to be fully opaque and
  covers the whole screen, stops processing of all strips below it
  (since they would not be visible anyway).
  Now the same optimization happens for some cases of strips that do not cover the whole screen:
  when a fully opaque strip completely covers some strip that is under it,
  the lower strip is not evaluated/rendered.
  (blender/blender@f4f708a54f9f)

# Video

- "AVI RAW" and "AVI JPEG" movie file output types are removed.
  Existing scenes using them will be updated to "FFmpeg Video" type with default options (H.264).
  (blender/blender@f09c7dc4ba58)
