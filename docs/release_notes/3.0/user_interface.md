# Blender 3.0: User Interface

## Theme and Widgets

- To celebrate the beginning of a new series, the default theme got a
  refresh.
  (blender/blender@bfec984cf82a6)
- Visual style update to panels.
  (blender/blender@93544b641bd6)
  - New theme setting for roundness.
  - Added margins to more clearly tell them apart.
  - Replace triangle with chevron icon.
- *Menu Item* now makes use of *Roundness* theme setting.
  (blender/blender@518365395152)
- *List Item* no longer relies on *Regular* buttons style when selected.
  (blender/blender@9e71a075473)
- Active *Tab* text now use region's *Text Highlight* theme color when
  active.
  (blender/blender@af26720b2131)
- Adjust editor's Header color when active (instead of when inactive).
  (blender/blender@962b17b3ca14)
- Style drag-drop indicators as tooltips.
  (blender/blender@499dbb626acb)
- Use arrow icon on context paths.
  (blender/blender@7c755293330e)
- Improve contrast on playhead.
  (blender/blender@452c78757f44)
- In Animation Editors and VSE, align vertical indicators to view.
  (blender/blender@3ccdee75328b)
- Use flat colors for NLA strips.
  (blender/blender@4758a7d35750)
- Remove separator lines between rows in VSE.
  (blender/blender@b4af70563ff1)

## Area Management

- Corner action zones allow joining any neighbors. Improved Header
  Context Menu.
  (blender/blender@8b049e4c2a53,
  blender/blender@c76141e425aa).

![](../../images/AreaJoins.gif){style="width:500px;"}

- New 'Area Close' operator.
  (blender/blender@8b049e4c2a53,
  blender/blender@06e62adfb8f2).

![](../../images/CloseOperator.png){style="width:500px;"}

- Mouse hit size for area resizing increased
  (blender/blender@84dcf12ceb7f).

![](../../images/BorderHitSize.png){style="width:500px;"}

- Improved mouse cursor feedback during Area Split and Join at
  unsupported locations.
  (blender/blender@3b1a16833b58).
- Resizing areas now snap (with Ctrl) to more consistent locations.
  (blender/blender@e5ed9991eaf0).

## General

- The editor header and tool settings region were swapped
  (blender/blender@4cf4bb2664ebe).
- If saving a file using CloseSave dialog, do not close if there is an
  error doing so, like if read-only.
  (blender/blender@cfa20ff03bde).
- Capture object thumbnails at an oblique angle for better preview of
  the shapes.
  (blender/blender@ecc7a837982e).

![](../../images/PreviewOrientation.png){style="width:500px;"}

- Do not create thumbnail previews of offline files, to avoid slow
  listings of Windows OneDrive folders.
  (blender/blender@ee5ad46a0ead).

![](../../images/OfflineFiles.png){style="width:500px;"}

- Fix console window briefly flashing when blender starts on windows.
  (blender/blender@f3944cf50396).
- Improved positioning of menu mnemonic underlines, especially when
  using custom font.
  (blender/blender@0fcc063fd99c,
  blender/blender@aee04d496035).
- "Render" Window now top-level (not child) on Windows platform, back to
  behavior prior to 2.93.
  (blender/blender@bd87ba90e639).
- Improved placement of child windows when using multiple monitors with
  any above any others (Win32).
  (blender/blender@d75e45d10cbf).
- Windows users can now associate current installation with Blend files
  in Preferences.
  (blender/blender@bcff0ef9cabc).

![](../../images/Blend_File_Association.png){style="width:500px;"}

- 3DView Statistics Overlay now shows Local statistics if you are in
  Local View
  (blender/blender@c8e331f45003).
- Improved scaling of File Browser preview images.
  (blender/blender@cb548329eaea).

![](../../images/Scaling2.png){style="width:700px;"}

- Do not resize temporary windows (Preferences, Render, etc) if they are
  already open.
  (blender/blender@643720f8abdb).
- Win32: Improved placement of windows when using multiple monitors that
  differ in DPI and scale.
  (blender/blender@999f1f75045c).
- macOS: support for Japanese, Chinese and Korean input in text fields
  (blender/blender@83e2f8c,
  blender/blender@0ef794b)
- Ctrl+F while hovering UI lists will open the search for the list
  (blender/blender@87c1c8112fa4).
- Text buttons are automatically scrolled into view when editing them
  (blender/blender@0c83ef567c50).
- Show descriptive display names for Fonts in File Browser instead of
  simple file names.
  (blender/blender@8aa1c0a326a8).

![](../../images/FontDisplayNames.png){style="width:500px;"}

- Show descriptive display names for 3D Text Object fonts.
  (blender/blender@b5bfb5f34c12).
- On Windows when entering Chinese or Japanese characters, do not
  duplicate initial keystroke.
  (blender/blender@836aeebf7077).
- Allow use of Wingdings and Symbol fonts in VSE text strips.
  (blender/blender@ae920d789ed3).
- Substantial speed increases for interface text drawing.
  (blender/blender@d5261e973b56,
  blender/blender@0d7aab2375e6)
- Less jiggling of contents when moving nodes.
  (blender/blender@400605c3a6a8).
- Increased resolution of blend file thumbnails.
  (blender/blender@bf0ac711fde2).

![](../../images/BlendThumbnailSizeComparison.png){style="width:500px;"}

- Improved blend preview thumbs, using screen captures or rendered with
  current current view shading.
  (blender/blender@58632a7f3c0f,
  blender/blender@4fa0bbb5ac3d).

![](../../images/BlendPreviews.png){style="width:500px;"}

- Split Output Properties Dimensions panel.
  (blender/blender@4ddad5a7ee5)
- File Browser "Favorites" section heading changed back to "Bookmarks"
  (blender/blender@a79c33e8f8f3)

## Custom Properties

- Rework the custom property edit operator to make it faster to use and
  more intuitive
  (blender/blender@bf948b2cef3ba3).

![Editing metadata from an array property with the new edit custom property operator UI.](../../images/Reworked_Custom_Property_Edit_Operator_2.png){style="width:500px;"}

- Improve the layout of custom property edit panel
  blender/blender@972677b25e1d84

## 3D Viewport

- Display indicator in *Text Info* overlay when *Clipping Region* is
  enabled
  (blender/blender@9c509a721949)
- Z axis lock option for the walk navigation
  (blender/blender@c749c24682159)
- Navigation gizmos no longer hide when using modal operators
  (blender/blender@917a972b56af10).
- The different outline color for selected instances has been removed,
  improving readability of the active object
  (blender/blender@aa13c4b386b131).

## Keymap

- Holding the `D` & dragging now opens the view menu (moved from the
  `Tilde` key). The "Switch to Object" operator is now mapped to the
  `Tilde` key.
  (blender/blender@f92f5d1ac62c66ceb7a6ac1ff69084fbd5e3614a).

## Outliner

- Filter option "All View Layers" shows all the view layers for
  comparing different collection values
  (blender/blender@bb2648ebf002).

![](../../images/Outliner_view_layers.png){style="width:500px;"}

## Freestyle

A major rework of the UI layout to make the layout align with the
current UI design principles
(blender/blender@6f52ebba192).

## Preview

- Sphere material preview updated to have squared UV and better poles
  (blender/blender@875f24352a76).

![Before](../../images/Sphere_preview_-_before.png){style="width:500px;"}
![After](../../images/Sphere_preview_-_after.png){style="width:500px;"}
