# Blender 3.0: VFX & Video

## Clip Editor

- Expose 2D Cursor Location to UI and Python API
  (blender/blender@2d867426b1b7)
- User Interface: Move Annotation Panel to new View tab
  (blender/blender@ce95a2b148e)
- Sort by start/end frame in the dopesheet
  (blender/blender@fb820496f5b0)

## VFX

- Updated camera presets
  (blender/blender@d486ee2dbdd)

## Sequencer

- 128 channels are supported now (before it was 32)
  (blender/blender@8fecc2a85254)
- The layout of the speed Effect Strip has been reworked
  (blender/blender@f013e3de81da)
- Allow effect inputs to be blended if placed above effect
  (blender/blender@0b7744f4da66)
- Bring back select strips at current frame feature
  (blender/blender@7655cc45eb92)
- Use own category for metadata panel
  (blender/blender@3400ba329e6b)
- Add refresh_all operator to all sequencer regions
  (blender/blender@3e695a27cdfa)
- Improve animation evaluation performance
  (blender/blender@1a5fa2b319e0)
- Reduce transform code complexity
  (blender/blender@e7003bc9654b)
- Improved Snapping
  (blender/blender@fba9cd019f21)
- Make pasted strip active
  (blender/blender@d374469f4c04)
- Add Sequence.split RNA function
  (blender/blender@53743adc297f)
- Draw strips transparent during transform overlap
  (blender/blender@f5cc3486107)
- Add Sequence.parent_meta() python API function
  (blender/blender@eec1ea0ccf2)
- Set default sound and video export format
  (blender/blender@70f890b5105)
- Add transform overwrite mode
  (blender/blender@59cd9c6da682)
- Strip thumbnails
  (blender/blender@997b5fe45dab)
- Image transform tools
  (blender/blender@fa2c1698b077)
- Add color tags to strips
  (blender/blender@5cebcb415e76)
- Add ASC CDL color correction method
  (blender/blender@213554f24a17)
