# Sculpt, Paint, Texture

## Features

- Add support for "Adjust Last Operation" panel to mesh & color filters
  (blender/blender@c352eeb2134)
  (blender/blender@b4ee9366278a8159af5e2b97bfc18ccc29354aa3)

![](../../images/Filter_repeat_last.png){style="width:400px;"}

- Transform, Trim, Project, Fairing and Filter operations are now also
  available in the header menu
  (blender/blender@da65b21e2e0)

This makes it possible to easily assign these operation a shortcut or
add them to the Quick Favorites menu for faster access.

Repeated use is possible with Repeat Last (`Shift+R`).

![](../../images/Sculpt_menu_update.png){style="width:300px;"}

- Added trim orientation to Box Trim UI. Previously only available in
  the Lasso Trim tool
  (blender/blender@a843a9c9bb8a25f123752fcd6ca68f4694974a44)
