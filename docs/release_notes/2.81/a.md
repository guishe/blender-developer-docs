## Blender 2.81: Bug Fixes

### Blender

- Fix [\#71147](http://developer.blender.org/T71147): Eevee stops
  rendering after selection attempt
  (blender/blender@4e42a98edd8)
- Fix [\#71213](http://developer.blender.org/T71213): Mask or Mirror
  before Armature breaks weight paint
  (blender/blender@f1ac64921b4
  )
- Fix [\#71612](http://developer.blender.org/T71612): Viewport rotate
  doesn't work
  (blender/blender@aadbb794cd6e)
- Fix [\#71741](http://developer.blender.org/T71741): Crash showing the
  object relations menu
  (blender/blender@8cb55f8d1672)
- Fix [\#71558](http://developer.blender.org/T71558): Hair particles:
  Brush effect not occluded by emitter geometry
  (blender/blender@a8d29ad6e06)
- Fix segfault when polling `MESH_OT_paint_mask_extract`
  (blender/blender@73ce35d3325)
- Fix [\#71864](http://developer.blender.org/T71864): Broken 'Select' \>
  'More' in face mode in UV Editor
  (blender/blender@bdfcee347eb)
- Fix [\#69332](http://developer.blender.org/T69332): 'Reset to Default
  Value' on a custom string property crashes
  (blender/blender@60e817693ce)
- Fix [\#72071](http://developer.blender.org/T72071): Crash on snap to
  edge
  (blender/blender@4a440ecb99d)
- Fix crash exiting edit-mode with an active basis shape key
  (blender/blender@34902f20089)

### Addons

- Fix [\#71774](http://developer.blender.org/T71774): SVG import error
  on specific files
  (blender/blender-addons@ee818b2a)
- Fix [\#71678](http://developer.blender.org/T71678): Rigify crash if
  the Advanced mode is set to New.
  (blender/blender-addons@ed5b81f5)
- Fix [\#72145](http://developer.blender.org/T72145): STL crash
  exporting object without data
  (blender/blender-addons@2f425cc1)
