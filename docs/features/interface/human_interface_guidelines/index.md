---
hide:
  - toc
---

# Human Interface Guidelines

### A common design language for Blender

The Human Interface Guidelines (HIG) help contributors make great user interfaces, with a common
design language throughout Blender. They give practical guidance, help avoid common pitfalls, and
show why certain patterns are chosen over others.

The HIG is a resource for all contributors. That includes core developers, add-on developers and
designers.

!!! Warning "Work in Progress"
    The Human Interface Guidelines are still in development. They are unpolished and a lot of
    content is missing still.

---


## Overview

- [Design Paradigms](paradigms.md): The broad patterns defining the design philosophy of Blender.
- [General Patterns](general_patterns.md): More specific patterns applying to the user interface as
  a whole. Covers topics like writing styles, colors, accessibility, icons, ... This is a great
  place to learn how to think about design on a practical level.
- [Components](components.md): Guides for the individual elements used in Blender to compose its user interface, and how to use them.
- [User Feedback](user_feedback.md): Helpful feedback can elevate the user experience to the next
  level. The guidelines here show effective ways to speak to users with more than just text.
- [Glossary](glossary.md): Important terms and definitions to make the language used for the HIG
  and to design the Blender user interface more precise, without ambiguity.


## Contribute

The HIG is created and maintained by the [User Interface Module](https://projects.blender.org/blender/blender/wiki/Module:%20User%20Interface). Contributions can follow the usual [contribution process for the developer documentation](../../../contribute/index.md), and tag reports, tasks and pull requests with _Module: User Interface_.
