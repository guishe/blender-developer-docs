## Cycles Papers

An (incomplete) list of papers that we based Cycles code on:

- Constructing Sobol sequences with better two-dimensional projections
  ([link](http://web.maths.unsw.edu.au/~fkuo/sobol/))
- Diploma Thesis Motion Blur
  ([pdf](http://gruenschloss.org/motion-blur/motion-blur.pdf))
- Ray Tracing Deformable Scenes using Dynamic Bounding Volume
  Hierarchies
  ([pdf](http://www.sci.utah.edu/~wald/Publications/2007///BVH/download//togbvh.pdf))
- Spatial Splits in Bounding Volume Hierarchies
  ([link](http://www.nvidia.com/object/nvidia_research_pub_012.html))
- Optimally Combining Sampling Techniques for Monte Carlo Rendering
  ([link](http://www-graphics.stanford.edu/papers/combine/))
- A Practical Analytic Model for Daylight
  ([link](http://www.cs.utah.edu/~shirley/papers/sunsky/))
- Microfacet Models for Refraction through Rough Surfaces
  ([pdf](http://www.cs.cornell.edu/~srm/publications/EGSR07-btdf.pdf))
- Predicting Reflectance Functions from Complex Surfaces
  ([link](http://www.graphics.cornell.edu/~westin/SIGGRAPH92.html))
- Measuring and Modeling Anisotropic Reflection
  ([link](http://radsite.lbl.gov/radiance/papers/sg92/paper.html))
- Notes on the Ward BRDF
  ([pdf](http://www.graphics.cornell.edu/~bjw/wardnotes.pdf))
- A Microfacet-based BRDF Generator
  ([link](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.18.2354))
- Tracing Ray Differentials
  ([link](http://www-graphics.stanford.edu/papers/trd/))
- What is the Space of Camera Response Functions?
  ([link](http://www.cs.columbia.edu/CAVE/software/softlib/dorf.php))
- Fast, minimum storage ray-triangle intersection
  ([link](http://www.graphics.cornell.edu/pubs/1997/MT97.html))
- Understanding the Efficiency of Ray Traversal on GPUs
  ([link](http://www.nvidia.com/object/nvidia_research_pub_011.html))
- A Ray Tracing Hardware Architecture for Dynamic Scenes
  ([link](http://www.sven-woop.de/publications.html))
- Improving Noise ([link](http://mrl.nyu.edu/~perlin/noise/))
- Texturing and Modelling: A procedural approach
